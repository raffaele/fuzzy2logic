(function () {
    var module = angular.module('fuzzy');

    module.factory('fuzzyVerifier', function () {
        return {getFuzzyFnGroupError: getFuzzyFnGroupError};
    });

    function getFuzzyFnGroupError (fuzzySet) {
        var hasUnnamedFn;
        if (!angular.isArray(fuzzySet)) {
            return 'NOT_AN_ARRAY';
        }
        if (fuzzySet.length < 2) {
            return 'MIN_LENGTH_2';
        }
        if (fuzzySet.length > 10) {
            return 'MAX_LENGTH_10';
        }
        
        return null;
    }
})();