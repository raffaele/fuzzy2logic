describe('fuzzyVerifier factory', function () {
    'use strict';
    var fuzzyVerifier;
    beforeEach(function () {
        module('fuzzy');
        inject(function ($injector) {
            fuzzyVerifier = $injector.get('fuzzyVerifier');
        });
    });

    it('should be defined', function () {
        expect(angular.isDefined(fuzzyVerifier)).toBe(true);
    });

    describe('getFuzzyFnGroupError method', function () {
        var getFuzzyFnGroupError;

        beforeEach(function () {
            getFuzzyFnGroupError = fuzzyVerifier.getFuzzyFnGroupError;
        });
        it('should be defined', function () {
            expect(angular.isDefined(getFuzzyFnGroupError)).toBe(true);
        });
        it('should report if the set is not an array', function () {
            expect(getFuzzyFnGroupError({})).toBe('NOT_AN_ARRAY');
        });
        it('should report short array 2', function () {
            expect(getFuzzyFnGroupError([{}])).toBe('MIN_LENGTH_2');
        });
        it('should report long array 2', function () {
            expect(getFuzzyFnGroupError([{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}])).toBe('MAX_LENGTH_10');
        });
        
    });
});