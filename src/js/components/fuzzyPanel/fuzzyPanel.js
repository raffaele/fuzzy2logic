(function () {
    'use strict';

    var module = angular.module('fuzzy');

    module.component('fuzzyPanel', {
        templateUrl: './js/components/fuzzyPanel/fuzzyPanel.html',
        controller: fuzzyPanelCtrl,
        bindings: {
            fuzzyModel: '<',
            onFuzzyClick: '&'
        }
    });

    function fuzzyPanelCtrl (fuzzyVerifier) {
        var $ctrl = this;

        $ctrl.reportClick = function (itemIndex) {
            if ($ctrl.onFuzzyClick) {
                $ctrl.onFuzzyClick({
                    index: itemIndex,
                    fuzzyFn: $ctrl.fuzzyModel[itemIndex]
                });
            }
        };

        $ctrl.$onChanges = function (changes) {
            if (changes.fuzzyModel) {
                updateFuzzyView();
            }
        };

        function updateFuzzyView () {
            $ctrl.fuzzyError = fuzzyVerifier.getFuzzyFnGroupError($ctrl.fuzzyModel);
            if ($ctrl.fuzzyError) {
                return;
            }
            $ctrl.minRange = getMinRange();
            $ctrl.rangeSize = getMaxRange() - $ctrl.minRange;
        }

        function getMaxRange () {
            var lastFuzzyFn = $ctrl.fuzzyModel.slice(-1).pop();
            return lastFuzzyFn.upperEnd;
        }

        function getMinRange () {
            return $ctrl.fuzzyModel[0].upperStart;
        }
    }
})();
