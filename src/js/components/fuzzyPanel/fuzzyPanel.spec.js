describe('fuzzyPanel controller', function () {
    'use strict';

    var template = '<fuzzy-panel fuzzy-model="model" on-fuzzy-click="reportMethod(fuzzyFn, index)"></fuzzy-panel>';

    var $scope, templateElement, ctrl;

    var fuzzyVerifier;

    beforeEach(function () {
        fuzzyVerifier = {
            getFuzzyFnGroupError: angular.noop
        };

        module('fuzzy', function ($provide) {
            $provide.factory('fuzzyVerifier', function () {
                return fuzzyVerifier;
            });
        });
        inject(function ($compile, $rootScope, $componentController, $templateCache) {
            $templateCache.put('./js/components/fuzzyPanel/fuzzyPanel.html', '<div></div>');
            $scope = $rootScope.$new();
            $scope.model = [{}, {}];
            $scope.reportMethod = angular.noop;
            templateElement = $compile(template)($scope);
        });
    });

    it('should report the error provided by fuzzyVerifier.getFuzzyFnGroupError', function () {
        var errorMsg = 'error';
        spyOn(fuzzyVerifier, 'getFuzzyFnGroupError').and.returnValue(errorMsg);
        $scope.$digest();
        ctrl = templateElement.isolateScope().$ctrl;
        expect(ctrl.fuzzyError).toBe(errorMsg);
    });

    it('should report no error on approval from fuzzyVerifier.getFuzzyFnGroupError', function () {
        var errorMsg = null;
        spyOn(fuzzyVerifier, 'getFuzzyFnGroupError').and.returnValue(errorMsg);
        $scope.$digest();
        ctrl = templateElement.isolateScope().$ctrl;
        expect(ctrl.fuzzyError).toBe(errorMsg);
    });

    it('should set the proper minRange/rangeSize param', function () {
        var model = [{upperStart: 10}, {}, {upperEnd: 1000}];
        spyOn(fuzzyVerifier, 'getFuzzyFnGroupError').and.returnValue(null);
        $scope.model  = model;
        $scope.$digest();
        ctrl = templateElement.isolateScope().$ctrl;
        expect(ctrl.minRange).toBe(10);
        expect(ctrl.rangeSize).toBe(990);
    });

    describe('showClick method', function () {
        var model,
            ctrl;
        beforeEach(function () {
            model = [{upperStart: 10}, {name: 'name'}, {upperEnd: 1000}];
            spyOn($scope, 'reportMethod');
            $scope.model  = model;
            $scope.$digest();
            ctrl = templateElement.isolateScope().$ctrl;
        });

        it('should fail', function () {
            ctrl.reportClick(1);
            expect($scope.reportMethod).toHaveBeenCalledWith(model[1], 1);
        });
    });
});
