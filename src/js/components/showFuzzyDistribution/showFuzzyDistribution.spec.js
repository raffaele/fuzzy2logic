describe('fuzzyPanel controller', function () {
    'use strict';

    var template = '<show-fuzzy-distribution membership-fn="membershipFn" range-min="min" range-size="scale"></show-fuzzy-distribution>';

    var $scope, templateElement, ctrl;

    beforeEach(function () {

        module('fuzzy');
        inject(function ($compile, $rootScope, $componentController, $templateCache) {
            $templateCache.put('./js/components/showFuzzyDistribution/showFuzzyDistribution.html', '<div></div>');
            $scope = $rootScope.$new();
            $scope.min = 10;
            $scope.scale = 500;
            $scope.membershipFn = {
                upperStart: 20,
                upperTop1: 50,
                upperTop2: 100,
                upperEnd: 200,

            };
            templateElement = $compile(template)($scope);
            $scope.$digest();
            ctrl = templateElement.isolateScope().$ctrl;
        });
    });

    it('should report the correct left empty space', function () {
        expect(ctrl.leftGapWidth).toBe(2);
    });

    it('should report the left fuzzy area size', function () {
        expect(ctrl.leftFuzzyWidth).toBe(6);
    });

    it('should report the correct core area', function () {
        expect(ctrl.coreAreaWidth).toBe(10);
    });

    it('should report the right fuzzy area size', function () {
        expect(ctrl.rightFuzzyWidth).toBe(20);
    });

    it('should report the correct right empty space', function () {
        expect(ctrl.rightGapWidth).toBe(62);
    });
});
