(function () {
    'use strict';
    var module = angular.module('fuzzy');

    module.component('showFuzzyDistribution', {
        templateUrl: './js/components/showFuzzyDistribution/showFuzzyDistribution.html',
        controller: showFuzzyDistCtrl,
        bindings: {
            membershipFn: '<',
            rangeMin: '<',
            rangeSize: '<'
        }
    });

    function showFuzzyDistCtrl () {
        var $ctrl = this;

        $ctrl.$onInit = function () {

            var start = getPercentageValue($ctrl.membershipFn.upperStart, $ctrl.rangeMin, $ctrl.rangeSize);
            var coreStart = getPercentageValue($ctrl.membershipFn.upperTop1, $ctrl.rangeMin, $ctrl.rangeSize);
            var coreEnd = getPercentageValue($ctrl.membershipFn.upperTop2, $ctrl.rangeMin, $ctrl.rangeSize);
            var end = getPercentageValue($ctrl.membershipFn.upperEnd, $ctrl.rangeMin, $ctrl.rangeSize);

            $ctrl.leftGapWidth = start;
            $ctrl.leftFuzzyWidth = coreStart - start;
            $ctrl.coreAreaWidth = coreEnd - coreStart;
            $ctrl.rightFuzzyWidth = end - coreEnd;
            $ctrl.rightGapWidth = 100 - end;

        };
        
        function getPercentageValue (value, start, scale) {
            var xCoord = value - start,
                scaledXCoord = xCoord/scale;
            
            return 100 * scaledXCoord;
        }
    }
})();
