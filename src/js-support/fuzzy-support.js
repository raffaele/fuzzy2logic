(function () {
    var module = angular.module('fuzzy-support', ['fuzzy']);

    module.component('fuzzyWrapper', {
        templateUrl: './js-support/fuzzy-support.html',
        controller: fuzzyWrapperCtrl
    });

    function fuzzyWrapperCtrl ($http) {
        var $ctrl = this;

        $ctrl.$onInit = function () {
            $ctrl.log = '';
        };

        $ctrl.showClick = function (fuzzyFn, fuzzyFnIndex) {
            var separator = $ctrl.log ? '\n\n' : '';
            $ctrl.log += (separator + JSON.stringify(fuzzyFn));
        }
        
        $http.get('./js-support/example.json').then(function (response) {
            $ctrl.models = [{
                label: 'no array',
                value: {}
            }, {
                label: 'short',
                value: [{}]
            }, {
                label: 'long',
                value: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
            }, {
                label: 'provided example',
                value: response.data
            }];
            $ctrl.selectedModel = $ctrl.models[0];
        });
    }
})();