module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        serve: {
            options: {
                port: 9000,
                serve: {
                    path: './dist'
                }
            }
        },
        clean: {
            dist: ['dist']
        },
        copy: {
            dist: {
                files: [{
                    expand: true,
                    src: ['js/**/*', '!js/**/*.spec.js', 'js-support/*'],
                    cwd: 'src/',
                    dest: 'dist/'
                }]
            }
        },
        includeSource: {
            options:{
                rename: function (dest, matchedSrcPath, options) {
                    if (matchedSrcPath.match(/\.scss$/)) {
                        return matchedSrcPath;
                    }
                    return matchedSrcPath.replace(/^dist\//, '');
                },
                templates: {
                    scss: {
                        scss: '@import "{filePath}";',
                        css: '@import "{filePath}";'
                    }
                }
            },
            dist: {
                files: [
                    {src: 'src/index.html', dest: 'dist/index.html'}
                ]
            }
        },
        bower_main: {
            dist: {
                options: {
                    dest: 'dist/bower_main'
                }
            }
        }
    });

    grunt.registerTask('compile', [
        'clean:dist',
        'bower_main:dist',
        'copy:dist',
        'includeSource:dist'
    ]);
};