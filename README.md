Dependencies:
=========

1. [node JS](https://nodejs.org/)
2. [Grunt](https://gruntjs.com/)
3. [Bower](https://bower.io/)

Structure of the project:

The required component is `fuzzyPanel` and is implemented in `src/js` folder. There is also the folder `src/js-support` for some extra file used to use the component.

What is in `src/js` is covered by unit test.

Setup the environment:
=======

1. install node
2. install bower
3. install grunt-cli
4. run `npm install` command (to install library for deployment)
5. run `bower install` command (to install angular)

Execute the script (after the environment is setup)
======

1. Execute `npm test` to run unit tests (written in karma/jasmine)
2. Execute `npm start` to run a node server and go to the page http://localhost:9000/index.html


Angular annotation
=====

I generally prefer to avoid manual injection like `module.service(['$http', function ($http) { ... }])`.

This project is not prepared for minification, but to let it work properly I suggest to use an automatic grunt plugin like [ng-annotate](https://www.npmjs.com/package/grunt-ng-annotate).

It's possible to see [here](https://github.com/sapientiam/ng-slimmer) an example of usage of ng-annotate (the project is not on my github but in the log is reported I'm the only one pushed on that repository).
